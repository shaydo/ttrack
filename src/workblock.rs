extern crate chrono;
use chrono::{DateTime, Duration, Local, NaiveTime, TimeZone};
use std::fmt;
use std::io;
use std::io::{BufRead, ErrorKind};

pub struct Work {
    blocks: Vec<WorkBlock>,
}

impl Work {
    pub fn parse(reader: Box<io::BufRead>) -> io::Result<Work> {
        let mut blocks: Vec<WorkBlock> = Vec::new();

        for line in reader.lines() {
            let line = line?;
            let wb = WorkBlock::new_from_str(&line)?;
            if wb.start == 0 {
                return Err(invalid_data("block start is not defined"));
            }
            if let Some(last) = blocks.last() {
                if last.end == 0 {
                    return Err(invalid_data("block is not terminated"));
                }
                if !wb.follows(&last) {
                    return Err(invalid_data("blocks are not sequential"));
                }
            }
            blocks.push(wb)
        }

        Ok(Work { blocks })
    }

    pub fn duration(&self) -> Duration {
        let mut sum = 0;
        for b in &self.blocks {
            sum += b.length()
        }

        Duration::seconds(sum)
    }

    pub fn duration_today(&self) -> Duration {
        self.duration_since(Local::today().and_hms(0, 0, 0).timestamp())
    }

    pub fn duration_since(&self, ts: i64) -> Duration {
        let mut sum = 0;
        for b in &self.blocks {
            sum += b.length_since(ts)
        }

        Duration::seconds(sum)
    }

    pub fn is_started(&mut self) -> bool {
        if let Some(last) = self.blocks.last() {
            if last.end == 0 {
                return true;
            }
        }
        false
    }

    pub fn start(&mut self, dt: &Option<String>) -> io::Result<()> {
        if let Some(last) = self.blocks.last() {
            if last.end == 0 {
                return Err(err_other("already started"));
            }
        }
        let new_block = match dt {
            Some(dt) => WorkBlock::new_from_str(&dt)?,
            None => WorkBlock::new_from_now(),
        };
        self.blocks.push(new_block);
        Ok(())
    }

    pub fn stop(&mut self, dt: &Option<String>) -> io::Result<()> {
        if self.blocks.is_empty() {
            return Err(err_other("not started"));
        }
        if let Some(last) = self.blocks.last_mut() {
            if last.end != 0 {
                return Err(err_other("already stopped"));
            }
            match dt {
                Some(dt) => last.stop_at(&dt),
                None => last.stop(),
            }?
        }
        Ok(())
    }
}

impl fmt::Display for Work {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for b in &self.blocks {
            writeln!(f, "{}", b)?;
        }
        Ok(())
    }
}

#[cfg(test)]
mod wtests {
    #[test]
    fn parse() {
        //
    }
}

fn err_other(descr: &str) -> io::Error {
    io::Error::new(ErrorKind::Other, descr)
}

fn invalid_data(descr: &str) -> io::Error {
    io::Error::new(ErrorKind::InvalidData, descr)
}

struct WorkBlock {
    start: i64,
    end: i64,
}

impl WorkBlock {
    fn new_from_str(s: &str) -> std::io::Result<WorkBlock> {
        let parts: Vec<&str> = s.split('\t').collect();
        if parts.len() != 1 && parts.len() != 2 {
            return Err(invalid_data("can not parse workblock from the string"));
        }
        let start = WorkBlock::parse_timestamp(parts[0])?;
        let end = if parts.len() > 1 {
            WorkBlock::parse_timestamp(parts[1])?
        } else {
            0
        };
        if end != 0 && start > end {
            return Err(invalid_data("start of the block is after the end"));
        }
        Ok(WorkBlock { start, end })
    }

    fn new_from_now() -> WorkBlock {
        WorkBlock {
            start: Local::now().timestamp(),
            end: 0,
        }
    }

    fn stop(&mut self) -> io::Result<()> {
        self.end = Local::now().timestamp();
        Ok(())
    }

    fn stop_at(&mut self, dt: &str) -> io::Result<()> {
        self.end = WorkBlock::parse_timestamp(dt)?;
        Ok(())
    }

    fn parse_timestamp(s: &str) -> io::Result<i64> {
        if s.is_empty() {
            return Ok(0);
        }
        match DateTime::parse_from_rfc3339(s) {
            Ok(dt) => Ok(dt.timestamp()),
            Err(_) => WorkBlock::parse_time(s),
        }
    }

    fn parse_time(s: &str) -> io::Result<i64> {
        match NaiveTime::parse_from_str(s, "%H:%M") {
            Ok(t) => match Local::today().and_time(t) {
                Some(dt) => Ok(dt.timestamp()),
                None => Err(invalid_data("invalid date time")),
            },
            Err(e) => Err(io::Error::new(ErrorKind::InvalidData, e)),
        }
    }

    fn length(&self) -> i64 {
        if self.start == 0 {
            0
        } else if self.end == 0 {
            Local::now().timestamp() - self.start
        } else {
            self.end - self.start
        }
    }

    fn length_since(&self, ts: i64) -> i64 {
        if self.start == 0 {
            return 0;
        }
        let till = if self.end == 0 {
            Local::now().timestamp()
        } else {
            self.end
        };
        if till < ts {
            return 0;
        }
        let from = if self.start < ts { ts } else { self.start };
        till - from
    }

    fn follows(&self, prev: &WorkBlock) -> bool {
        self.start >= prev.end
    }
}

fn format_timestamp(ts: i64) -> String {
    if ts == 0 {
        "".to_string()
    } else {
        Local.timestamp(ts, 0).to_rfc3339()
    }
}

impl fmt::Display for WorkBlock {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if self.end == 0 {
            write!(f, "{}", format_timestamp(self.start))
        } else {
            write!(
                f,
                "{}\t{}",
                format_timestamp(self.start),
                format_timestamp(self.end)
            )
        }
    }
}

#[cfg(test)]
mod wbtests {
    #[test]
    fn parse_str() {
        let wb1r = super::WorkBlock::new_from_str("2019-01-01T06:00:00+01:00");
        assert!(wb1r.is_ok());
        let wb1 = wb1r.unwrap();
        assert_eq!(wb1.start, 1546318800);
        assert_eq!(wb1.end, 0);

        let wb2r =
            super::WorkBlock::new_from_str("2019-01-01T06:00:00+01:00\t2019-01-01T07:00:00+01:00");
        assert!(wb2r.is_ok());
        let wb2 = wb2r.unwrap();
        assert_eq!(wb2.start, 1546318800);
        assert_eq!(wb2.end, 1546322400);

        let wb3r =
            super::WorkBlock::new_from_str("2019-01-01T08:00:00+01:00\t2019-01-01T07:00:00+01:00");
        assert!(wb3r.is_err());

        let wb4r = super::WorkBlock::new_from_str("random text");
        assert!(wb4r.is_err());
    }

    #[test]
    fn parse_time() {
        let today_start = super::Local::today().and_hms(10, 44, 59).timestamp();
        let wb1r = super::WorkBlock::new_from_str("10:45");
        assert!(wb1r.is_ok());
        let wb1 = wb1r.unwrap();
        assert!(wb1.start > today_start);
        let end_check = super::Local::today().and_hms(10, 45, 1).timestamp();
        assert!(wb1.start < end_check)
    }

    #[test]
    fn length() {
        let wb1r = super::WorkBlock::new_from_str("2019-01-01T06:00:00+01:00");
        assert!(wb1r.is_ok());
        let mut wb1 = wb1r.unwrap();
        assert!(wb1.stop_at("2019-01-01T07:00:00+01:00").is_ok());
        assert_eq!(wb1.length(), 3600);

        let mut wb2 = super::WorkBlock::new_from_now();
        wb2.stop().expect("oops");
        assert!(wb2.length() < 5);

        assert!(wb2.follows(&wb1));
        assert!(!wb1.follows(&wb2));
    }
}
