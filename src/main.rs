extern crate chrono;
extern crate getopts;
use chrono::Duration;
use getopts::{Matches, Options, ParsingStyle};
use std::env;
use std::fs::File;
use std::io;
use std::io::{BufReader, ErrorKind, Write};

mod workblock;

fn main() {
    let args: Vec<String> = env::args().collect();
    let progname = args[0].clone();

    let mut opts = Options::new();
    opts.parsing_style(ParsingStyle::StopAtFirstFree)
        .reqopt("d", "", "file in which times are stored", "FILE")
        .optflag("h", "help", "print this help menu");

    match opts.parse(&args[1..]) {
        Ok(m) => {
            if m.opt_present("h") {
                print_usage(&progname, &opts);
                return;
            }
            if let Err(e) = run(&m) {
                println!("{}", e);
                std::process::exit(1);
            }
        }
        Err(f) => {
            println!("{}", f);
            print_usage(&progname, &opts);
            std::process::exit(1);
        }
    }
}

fn print_usage(progname: &str, opts: &Options) {
    let brief = format!("Usage: {} [options] start|stop|status", progname);
    print!("{}", opts.usage(&brief));
}

#[derive(Clone, Copy, PartialEq)]
enum Command {
    Status,
    Start,
    Stop,
}

fn run(m: &Matches) -> io::Result<()> {
    let filename = m.opt_str("d").unwrap();
    let command = if !m.free.is_empty() {
        match m.free[0].as_ref() {
            "status" => Command::Status,
            "start" => Command::Start,
            "stop" => Command::Stop,
            _ => return Err(err_other("invalid command")),
        }
    } else {
        Command::Status
    };

    let dt = if m.free.len() > 1 {
        Some(m.free[1].clone())
    } else {
        None
    };

    let r = open_file(&filename, command)?;
    let mut work = workblock::Work::parse(r)?;

    match command {
        Command::Start => {
            work.start(&dt)?;
            write_work(&filename, &work)?
        }
        Command::Stop => {
            work.stop(&dt)?;
            write_work(&filename, &work)?
        }
        _ => (),
    }

    if work.is_started() {
        println!("started")
    } else {
        println!("stopped")
    }
    print_duration("work total", work.duration());
    print_duration("work today", work.duration_today());

    Ok(())
}

fn print_duration(descr: &str, dur: Duration) {
    let hours = dur.num_hours();
    let minutes = dur.num_minutes() - 60 * hours;
    println!("{}: {}:{:02}", descr, hours, minutes);
}

fn err_other(descr: &str) -> io::Error {
    io::Error::new(ErrorKind::Other, descr)
}

fn write_work(filename: &str, work: &workblock::Work) -> std::io::Result<()> {
    let tempfile = filename.to_owned() + ".tmp";
    {
        let mut f = File::create(&tempfile)?;
        write!(f, "{}", work)?;
    }
    std::fs::rename(tempfile, filename)?;

    Ok(())
}

fn open_file(filename: &str, command: Command) -> io::Result<Box<io::BufRead>> {
    match File::open(filename) {
        Ok(f) => Ok(Box::new(BufReader::new(f))),
        Err(e) => {
            if command == Command::Start && e.kind() == ErrorKind::NotFound {
                match File::create(filename) {
                    Ok(_) => Ok(Box::new(io::empty())),
                    Err(e) => Err(e),
                }
            } else {
                Err(e)
            }
        }
    }
}
